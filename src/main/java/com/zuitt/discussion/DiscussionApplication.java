package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	// Activity
	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved";
	}

	@PostMapping("/users")
	public String createUser(){
		return "New user created.";
	}
	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid){
		return "User "+userid+" Retrieved";
	}
	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable Long userid,@RequestHeader(value="Authorization")String user){
		if (user == null || user.isEmpty()) {
			return "Unauthorized access.";
		}
		else{
			return "The User "+userid+" is deleted successfully";
		}
	}
	@PutMapping("/users/{userid}")
	@ResponseBody
	public User updateUser(@PathVariable Long userid,@RequestBody User user){
		return user;
	}


}
